import java.util.HashMap;
import java.util.Map;

/**
 * A period or block when a class may be scheduled.
 * Hard-coded using the upper-school bell schedule.
 */
public enum Block {
	A1(0),
	A2(1),
	A3(2),
	A4(3),
	B1(4),
	B2(5),
	B3(6),
	B4(7),
	UNASSIGNED(8);
	
	private int value;
	private static Map<Integer, Block> map = new HashMap<>();
	
	/**
	 * The number of actual different blocks in the schedule.
	 */
	public static int NBLOCKS = 8;
	
	private Block(int value) {
		this.value = value;
	}
	
	static {
		for (Block block : Block.values()) {
			map.put(block.value, block);
		}
	}
	
	/**
	 * @param val the integer value representing a particular block.
	 * @return the Block associated with that value.
	 */
	public static Block valueOf(int val) {
		return (Block) map.get(val);
	}
	
	/**
	 * @return the integer value representing a particular block.
	 */
	public int getValue() {
		return value;
	}
	
	public String toString() {
		switch(value) {
			case 0:
				return "A1";
			case 1:
				return "A2";
			case 2:
				return "A3";
			case 3:
				return "A4";
			case 4:
				return "B1";
			case 5:
				return "B2";
			case 6:
				return "B3";
			case 7:
				return "B4";
			default:
				return "unassigned";	
		}
	}
	
	/**
	 * @param id a case-sensitive (uppercase) string representing a particular block.
	 * @return the Block associated with that string, or the UNASSIGNED block.
	 */
	public static Block fromString(String id) {
	  switch(id) {
	    case "A1":
	      return A1;
	    case "A2":
	      return A2;
	    case "A3":
	      return A3;
	    case "A4":
	      return A4;
      case "B1":
        return B1;
      case "B2":
        return B2;
      case "B3":
        return B3;
      case "B4":
        return B4;
      default:
        return UNASSIGNED;
	  }
	}
	
	/**
	 * @return true if the Block is on an A day.
	 */
	public boolean isADay() {
		return value < 4;
	}
	
	/**
	 * @return true if the Block is on a B day.
	 */
	public boolean isBDay() {
		return value >= 4 && value < 8;
	}
}
