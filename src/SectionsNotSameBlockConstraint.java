import java.util.ArrayList;

/**
 * certain Sections should not be scheduled during the same Block.
 * For example, no two Sections taught by the same Instructor can be taught at the same time.
 * Note: a Constraint will be created for each Section of the same name as specified in the constraints.csv file.
 */
public class SectionsNotSameBlockConstraint implements Constraint {

	private Section section1;
	private Section section2;
	private int weight;
	
	public SectionsNotSameBlockConstraint(Section section1, Section section2, int weight) {
		this.section1 = section1;
		this.section2 = section2;
		this.weight = weight;
		
		section1.addConstraint(this);
		section1.getInstructor().addConstraint(this);
		section2.addConstraint(this);
		section2.getInstructor().addConstraint(this);
	}
	
	/**
	 * @return true if both Sections are assigned to the same Block
	 */
	public boolean isViolated() {
		if (section1 == null || section2 == null) return false;
		
		return (section1.getBlock() != Block.UNASSIGNED && 
				section1.getBlock().getValue() == section2.getBlock().getValue());
	}

	public int weight() {
		return weight;
	}

	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<>();
		
		if (section1 != null && section1.getInstructor() != null)
			retval.add(section1.getInstructor());
		
		if (section2 != null && section2.getInstructor() != null)
			retval.add(section2.getInstructor());
		
		return retval;
	}

	public ArrayList<Section> sections() {
		ArrayList<Section> retval = new ArrayList<>();
		
		if (section1 != null)
			retval.add(section1);
		
		if (section2 != null)
			retval.add(section2);
		
		return retval;
	}

	public String description() {		
		return "The following sections should not be at the same time: " + section1 + " and " + section2;
	}

}
