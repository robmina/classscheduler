import java.util.ArrayList;

/**
 * when two or more Instructors should have the same planning period (prep).
 */
public class MatchingPrepBlockConstraint implements Constraint {

	private ArrayList<Instructor> instructors;
	private int weight;
	
	public MatchingPrepBlockConstraint(ArrayList<Instructor> instructors, int weight) {
		this.instructors = instructors;
		this.weight = weight;
		
		for (Instructor i : instructors) {
		  i.addConstraint(this);
		}
	}
	
	/**
	 * @return true unless there is a Block during which all of the Instructors have a planning period (prep)
	 */
	public boolean isViolated() {
		if (instructors == null || instructors.size() <= 1)
			return false;
		
		for (int i = 0; i < Block.NBLOCKS; i++) {
			boolean overlap = true;
			for (Instructor inst: instructors) {
				if (inst == null) continue;
				
				if (inst.getSection(Block.valueOf(i)) != null) {
					overlap = false;
					break;
				}
			}
			if (overlap) return false;
		}
		
		return true;
	}

	public int weight() {
		return weight;
	}

	public ArrayList<Instructor> instructors() {
		return instructors;
	}

	public ArrayList<Section> sections() {
		return new ArrayList<Section>();
	}

	public String description() {
		if (instructors == null || instructors.size() == 0) return "";
		
		String retval = "The following instructors should have an overlapping planning block: ";

		for (int i = 0; i < instructors.size() - 1; i++) {
			retval += instructors.get(i) + ", ";
		}
		if (instructors.size() > 0) {
			if (instructors.size() > 1) retval += "and ";
			retval += instructors.get(instructors.size() - 1);
		}
		
		return retval;
	}

}
