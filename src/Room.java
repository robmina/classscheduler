import java.util.ArrayList;
import java.util.Objects;

/**
 * a classroom in which Sections may be taught.
 * The Schedule consists of Sections assigned to Rooms and Blocks.
 * Currently, each Room is available for every possible Block,
 * although this may not be true in practice.
 * @see Block.NBLOCKS
 */
public class Room {
  /**
   * a control designed to simulate the C++ friend keyword.
   * Taken from https://stackoverflow.com/a/18634125
   */
	public static final class RoomAssignControl { private RoomAssignControl() {} }
	private static final RoomAssignControl control = new RoomAssignControl();
	
	/**
	 * Room names are unique.
	 */
	private String name;
	/**
	 * redundant references to the Sections assigned to this Room.
	 * This is redundant with the reference stored in the Room objects.
	 */
	private ArrayList<Section> sections;
	
	public Room(String name) {
		this.name = name;
		this.sections = new ArrayList<>();
		
		// initialize sections with 8 elements
		for (int i = 0; i < Block.NBLOCKS; i++)
			this.sections.add(null);
	}
	
	public String getName() { return name; }
	public ArrayList<Section> getSections() { return sections; }
	
	/**
	 * @param block the Block
	 * @return the Section (if any) assigned during this particular Block
	 */
	public Section getSection(Block block) {		
		if (block.getValue() < sections.size())
			return sections.get(block.getValue());
		return null;
	}
	
	/**
	 * @param block the Block
	 * @return true if there is no Section assigned during this particular Block
	 */
	public boolean isOpen(Block block) {		
		return getSection(block) == null;
	}
	
	/**
	 * @return a List of Blocks with no assigned Section in this Room
	 */
	public ArrayList<Block> openBlocks() {
		ArrayList<Block> retval = new ArrayList<>();
		for (int i = 0; i < Block.NBLOCKS; i++) {
			Block b = Block.valueOf(i);
			if (isOpen(b)) retval.add(b);
		}
		return retval;
	}
	
	/**
	 * this is the only method callable from the Schedule class that makes Section assignments
	 * @param block the Block during which the Section will be taught
	 * @param section the Section to assign to this Room and Block
	 */
	public void assignSection(Block block, Section section) {
		Objects.requireNonNull(section);
		
		if (block.getValue() < sections.size()) {
			sections.set(block.getValue(), section);
			
			section.assignRoomAndBlock(control, this, block);
		}
	}
	/**
	 * this is the only method callable from the Schedule class that unmakes Section assignments
	 * @param block the Block to unassign
	 */
	public void unassignSection(Block block) {		
		if (block.getValue() < sections.size()) {
			Section section = sections.get(block.getValue());
			sections.set(block.getValue(), null);
			if (section != null) {
				section.unassignRoomAndBlock(control);
			}
		}
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Room && ((Room) o).getName() == this.name );
	}
	
	@Override
	public String toString() {
		return "Room " + this.name;
	}
}
