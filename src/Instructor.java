import java.util.ArrayList;
import java.util.Objects;

/**
 * a teacher/instructor of one or more class Sections.
 */
public class Instructor implements Costs {

	/**
	 * all the Constraints on the Schedule directly relevant to this Instructor.
	 * Used for convenience only, when printing summary information about the schedule.
	 */
	private ArrayList<Constraint> constraints;
	/**
	 * redundant references to the Sections taught by this Instructor.
	 * This is redundant with the reference stored in the Section objects.
	 */
	private ArrayList<Section> sections;
	/**
	 * Instructor names are unique.
	 */
	private String name;
	/**
	 * for convenience, the Sections taught by this Instructor sorted by Block.
	 */
	private ArrayList<Section> sectionsByBlock;
	
	/**
	 * @param name the unique name of this Instructor.
	 */
	public Instructor(String name) {
		constraints = new ArrayList<Constraint>();
		sections = new ArrayList<Section>();
		this.name = name;
		sectionsByBlock = new ArrayList<Section>(Block.NBLOCKS);

		// initialize sectionsByBlock with the right number of blocks
		for (int i = 0; i < Block.NBLOCKS; i++)
			sectionsByBlock.add(null);
	}
	
	public String getName() { return name; }
	public ArrayList<Constraint> getConstraints() { return constraints; }
	public ArrayList<Section> getSections() { return sections; }
	/**
	 * @param block the Block
	 * @return the Section assigned to that Block
	 */
	public Section getSection(Block block) {		
		if (block.getValue() < Block.NBLOCKS) {
			return sectionsByBlock.get(block.getValue());
		}
		return null;
	}
	/**
	 * @return the List of Blocks during which this Instructor has no assigned Sections
	 */
	public ArrayList<Block> freeBlocks() {
		ArrayList<Block> retval = new ArrayList<Block>();
		
		for (int i = 0; i < Block.NBLOCKS; i++) {
			if (sectionsByBlock.get(i) == null) 
				retval.add(Block.valueOf(i));
		}
		
		return retval;
	}
	/**
	 * @param b the Block
	 * @return true if the Instructor currently has a planning period during the Block
	 */
	public boolean isFree(Block b) {
	  return getSection(b) == null;
	}

	/**
	 * cost for all Constraints directly associated with this Instructor's assignments.
	 * For convenience, used when printing summary information about the Schedule.
	 */
	public int totalCost() { 
		int cost = 0;
		for ( Constraint c : constraints ) {
			if (c.isViolated()) cost += c.weight();
		}
		return cost;
	}

	@Override
	public String toString() {
		return "Instructor " + name;
	}
	
	/**
	 * @param c a control to prevent this method from being called except by the Section class
	 * @param s the Section to assign
	 * @param b the Block to assign the Section to
	 */
	public void assignBlock(Section.SectionAssignControl c, Section s, Block b) {
		Objects.requireNonNull(c);
		
		if (!sections.contains(s)) return;
		
		if (b.getValue() < Block.NBLOCKS) {
			sectionsByBlock.set(b.getValue(), s);
		}
	}
	
	/**
	 * @param c a control to prevent this method from being called except by the Section class
	 * @param s the Section to unassign
	 */
	public void unassignBlock(Section.SectionAssignControl c, Section s) {
		Objects.requireNonNull(c);
		
		if (!sections.contains(s)) return;
		
		for (int i = 0; i < Block.NBLOCKS; i++) {
			if (sectionsByBlock.get(i) == s) {
				sectionsByBlock.set(i, null);
				break;
			}
		}
	}
	
	/**
	 * called by the Constraint class constructor, not by Schedule.
	 * @param c Constraint to add to the List
	 */
	public void addConstraint(Constraint c) {
	  constraints.add(c);
	}
}
