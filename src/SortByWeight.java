import java.util.Comparator;

/**
 * Helper class to sort Constraint objects in either ascending or descending order of weight
 */
public class SortByWeight implements Comparator<Constraint> {
  
  public enum SortDirection {
    ASCENDING,
    DESCENDING
  }
  
  private SortDirection dir;
  
  public SortByWeight(SortDirection dir) {
    this.dir = dir;
  }
  
  public int compare(Constraint a, Constraint b) {
    if (dir == SortDirection.ASCENDING)
      return a.weight() - b.weight();
    return b.weight() - a.weight();
  }
}
