import java.util.ArrayList;

/**
 * certain Sections should be taught in particular Rooms.
 * Note: a Constraint will be created for each instance of Section with the same name as specified in the constraints.csv file.
 */
public class SpecificRoomConstraint implements Constraint {

	private ArrayList<Room> allowedRooms;
	private Section section;
	private int weight;
	
	public SpecificRoomConstraint(Section section, ArrayList<Room> allowedRooms, int weight) {
		this.allowedRooms = allowedRooms;
		this.section = section;
		this.weight = weight;
		
		this.section.addConstraint(this);
		this.section.getInstructor().addConstraint(this);
	}
	
	/**
	 * @return true if the Section is not assigned to one of the allowed Rooms
	 */
	public boolean isViolated() {
		if (section == null || allowedRooms == null)
			return false;

		if (allowedRooms.size() == 0 || section.getRoom() == null)
			return true;
		
		return !allowedRooms.contains(section.getRoom());
	}
	
	public int weight() {
		return this.weight;
	}
	
	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<>();
		if (section != null && section.getInstructor() != null)
			retval.add(section.getInstructor());
		return retval;
	}
	
	public ArrayList<Section> sections() {
		ArrayList<Section> retval = new ArrayList<>();
		if (section != null)
			retval.add(section);
		return retval;
	}
	
	public String description() {
		if (allowedRooms == null || section == null) return "";
		
		String retval = "" + section + " should be in one of the following rooms: ";
		
		for (int i = 0; i < allowedRooms.size() - 1; i++) {
			retval += allowedRooms.get(i) + ", ";
		}
		if (allowedRooms.size() > 0) {
			if (allowedRooms.size() > 1)
				retval += "and ";
			retval += allowedRooms.get(allowedRooms.size() - 1);
		}
		
		return retval;
	}
}
