import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * A full schedule, encapsulating all instructors, sections, rooms, and constraints.
 * Also contains the code for parsing input data files, optimizing the schedule, and writing summary data about the schedule.
 */
public class Schedule implements Costs {
	private ArrayList<Instructor> instructors;
	private ArrayList<Section> sections;
	private ArrayList<Room> rooms;
	private ArrayList<Constraint> constraints;
	private String dataDir;
	private int nWaySwaps;
	private long maxPermutations;
	private HashSet<String> skipPrepBlockEachDayNames;

	public static void main(String[] args) {
	  Schedule theSchedule = new Schedule("data/", 2, 1000000);
	  
	  theSchedule.printAllSummaryData("./before/");
	  theSchedule.initialAssignment();
	  theSchedule.printAllSummaryData("./initial/");
	  theSchedule.nWaySwapOptimization();
	  theSchedule.swapIntoOpenRooms();
	  theSchedule.printAllSummaryData("./after/");
	}
	
	/**
	 * @param fname path to the output file
	 * @param str string to write into the file
	 */
	public static void printToFile(String fname, String str) {
	  try (PrintStream out = new PrintStream(new FileOutputStream(fname))) {
	    out.print(str);
	  } catch (FileNotFoundException e) {
	    System.out.println("Warning: could not write file " + fname);
	  }
	}
	
	/**
	 * @param outputDir path to the output directory to write summary data into
	 */
	public void printAllSummaryData(String outputDir) {
    printToFile(outputDir + "/schedule_instructor.csv", scheduleByInstructorCSV());
    printToFile(outputDir + "/schedule_room.csv", scheduleByRoomCSV());
    printToFile(outputDir + "/sections_summary.csv", sectionSummaryCSV());
    printToFile(outputDir + "/instructors_summary.csv", instructorSummaryCSV());
    printToFile(outputDir + "/constraints_summary.csv", constraintSummaryCSV());	  
	}
	
	/**
	 * @param dataDirectory directory path containing the input .csv files (instructors.csv, sections.csv, rooms.csv, constraints.csv)
	 * @param nWaySwaps cap on the number of sections that may be permuted when optimizing the schedule (for each set of n sections, the algorithm exhaustively checks all n! permutations). If this value is under 2, no optimization is performed.
	 * @param maxPerms cap on the total number of permutations to consider when optimizing the schedule, after which the algorithm will stop trying new permutations
	 */
	public Schedule(String dataDirectory, int nWaySwaps, long maxPerms) {
		instructors = new ArrayList<>();
		sections = new ArrayList<>();
		rooms = new ArrayList<>();
		constraints = new ArrayList<>();
		
		dataDir = dataDirectory;
		
		this.nWaySwaps = nWaySwaps;
		this.maxPermutations = maxPerms;
		
		skipPrepBlockEachDayNames = new HashSet<>();
		
		readRooms();
		readInstructors();
		readSections();
		readConstraints();
		autogenerateConstraints();
	}
	
	/**
	 * @return the total cost of all violated constraints configured in the input file constraints.csv
	 */
	public int totalCost() { 
		int cost = 0;
		for ( Constraint c : constraints ) {
			if (c.isViolated()) cost += c.weight();
		}
		return cost;
	}
	
	/**
	 * @return true if every section has been assigned to a room and block
	 */
	public boolean fullyAssigned() {
		for (Section s : sections) {
			if (s.getBlock() == Block.UNASSIGNED || s.getRoom() == null)
				return false;
		}
		return true;
	}
	
	public void optimizeSchedule() {
	  initialAssignment();
	  nWaySwapOptimization();
	}
	
	/**
	 * Perform an initial pass over the sections, in descending order of cost,
	 * and assign to the best available room and block.
	 */
	private void initialAssignment() {
		if (rooms.size() == 0) return; // nothing to do
		
		// sort sections by difficulty to schedule
		Collections.sort(sections, new SortByCost(SortByCost.SortDirection.DESCENDING));
		// assign each section in decreasing order of difficulty
		for (Section s : sections) {
			int minCost = Integer.MAX_VALUE;
			Room minCostRoom = rooms.get(0);
			Block minCostBlock = Block.UNASSIGNED;
			for (Room r : rooms) {
				boolean foundMin = false;
				for (Block b : r.openBlocks()) {
				  if (!s.getInstructor().isFree(b)) continue;
				  
					r.assignSection(b, s);
					int cost = s.totalCost();
					if (cost < minCost) {
						minCost = cost;
						minCostRoom = r;
						minCostBlock = b;
						if (cost == 0) { // assuming there are no negative costs, 0 is the minimum
							foundMin = true;
							break;
						}
					}
					r.unassignSection(b);
				}
				if (foundMin) break; // found the absolute minimum
			}
			minCostRoom.assignSection(minCostBlock, s);
		}
	}
	
	/**
	 * Minimize the total cost of the schedule.
	 * Loop over all possible sets of n sections in descending order of cost, finding the optimal permutation of those n by swapping amongst them.
	 * After optimizing each set, move sections into unoccupied rooms/blocks if this reduces the total cost.
	 * If a set is optimized by performing a swap, resort and restart the loop until all n-way sets are optimized, or a termination condition is reached.
	 * The loop will terminate when: the absolute minimum cost (0) is reached, the total permutations cap is surpassed, or all n-way permutations have been checked.
	 */
	private void nWaySwapOptimization() {
	  if (!fullyAssigned()) return; // need to be assigned first
	  
	  if (nWaySwaps <= 1) return; // nothing to do
	  
	  // sort by current cost, descending order
	  Collections.sort(sections, new SortByCost(SortByCost.SortDirection.DESCENDING));
	  // how many n-way swaps are possible?
	  long nSwaps = PermuteUtils.binomial(sections.size(), nWaySwaps);
	  // how many permutations are possible within each swap?
	  long nPerms = PermuteUtils.factorial(nWaySwaps);
	  int currentCost = totalCost();
	  long totalPerms = 0L;
	  for (long iSwap = 0; iSwap < nSwaps; iSwap++) {
	    ArrayList<Section> swapSections = PermuteUtils.getCombination(sections, nWaySwaps, iSwap);
	    
	    int minCost = currentCost;
	    ArrayList<Section> swapOrder = (ArrayList<Section>) swapSections.clone();
	    ArrayList<Section> minCostSwapOrder = (ArrayList<Section>) swapOrder.clone();
	    // we can skip the 0th permutation, which is the same as the original
	    for (long iPerm = 1; iPerm < nPerms; iPerm++) {
	      totalPerms++;
	      swapOrder = PermuteUtils.getPermutation(swapSections, iPerm);
	      applySwap(swapSections, swapOrder);
	      int cost = totalCost();
	      applySwap(swapOrder, swapSections); // revert the swap
	      if (cost < minCost) {
	        minCost = cost;
	        minCostSwapOrder = swapOrder;
	        if (cost == 0) { // don't need to continue looping since 0 is the absolute min
	          break;
	        }
	      }
	      
	      if (totalPerms > maxPermutations) break;
	    }
	    
	    if (minCost < currentCost) {
	      applySwap(swapSections, minCostSwapOrder);
	      
	      // search for a swap into an unoccupied room/block
	      swapIntoOpenRooms();
	      
	      // recalculate cost
	      currentCost = totalCost();
	      // resort the list
        Collections.sort(sections, new SortByCost(SortByCost.SortDirection.DESCENDING));
	      // reset the search index
	      iSwap = -1;
	    }
	    
	    if (currentCost == 0) return; // assuming no negative costs, 0 is the absolute min
	    
	    if (totalPerms > maxPermutations) break;
	  }
	}
	
	/**
	 * An intermediate step for optimizing the schedule.
	 * Move sections into unoccupied rooms/blocks as possible to reduce the total cost.
	 */
	private void swapIntoOpenRooms() {
	  if (!fullyAssigned()) return; // must be fully assigned before optimizing
	  
	  // sort by current cost, descending order
	  Collections.sort(sections, new SortByCost(SortByCost.SortDirection.DESCENDING));
	  
	  int currentCost = totalCost();
	  
	  for (Section s : sections) {
      Room oldRoom = s.getRoom();
      Block oldBlock = s.getBlock();
      oldRoom.unassignSection(oldBlock);

	    int minCost = currentCost;
	    Room minCostRoom = oldRoom;
	    Block minCostBlock = oldBlock;
	    for (Room r : rooms) {
	      boolean minFound = false;
	      for (Block b : r.openBlocks()) {
	        if (!s.getInstructor().isFree(b)) continue;
	        
	        r.assignSection(b, s);
	        int cost = totalCost();
	        if (cost < minCost) {
	          minCost = cost;
	          minCostRoom = r;
	          minCostBlock = b;
	          if (cost == 0) { // 0 is the absolute min
	            minFound = true;
	            break;
	          }
	        }
	        r.unassignSection(b);
	      }
	      if (minFound) break;
	    }
	    
	    if (minCost < currentCost) {
	      currentCost = minCost;
	      minCostRoom.assignSection(minCostBlock, s);
	    } else {
        oldRoom.assignSection(oldBlock, s);
	    }
	    
	    if (currentCost == 0) return; // assuming no negative costs, 0 is the absolute min
	  }
	}
	
	/**
	 * Swap the room/block assignments of a set of sections.
	 * @param original the original order of the section assignments.
	 * @param swapped the desired order of the section assignments after swapping.
	 */
	private void applySwap(ArrayList<Section> original, ArrayList<Section> swapped) {
	  if (original.size() != swapped.size()) return; // bad
	  
	  // unassign and store room and block
	  ArrayList<Room> roomOrder = new ArrayList<>(original.size());
	  ArrayList<Block> blockOrder = new ArrayList<>(original.size());
	  for (Section s : original) {
	    Room r = s.getRoom();
	    Block b = s.getBlock();
	    roomOrder.add(r);
	    blockOrder.add(b);
	    r.unassignSection(b);
	  }
	  // reassign
	  for (int i = 0; i < swapped.size(); i++) {
	    Room r = roomOrder.get(i);
	    Block b = blockOrder.get(i);
	    Section s = swapped.get(i);
	    r.assignSection(b, s);
	  }
	}
	
	/**
	 * Taken from https://www.baeldung.com/java-csv-file-array
	 * @param fname path to the csv file
	 * @return a list of records (lines), each of which is a {@code List<String>}
	 */
	private List<List<String>> readCSV(String fname) {
	  List<List<String>> records = new ArrayList<>();
	  try (Scanner scanner = new Scanner(new File(fname))) {
	    while (scanner.hasNextLine()) {
	      records.add(getRecordFromLine(scanner.nextLine()));
	    }
	    scanner.close();
	  } catch (FileNotFoundException f) {
	    f.printStackTrace();
	  }
	  return records;
	}
	/**
	 * Taken from https://www.baeldung.com/java-csv-file-array
	 * called from readCSV
	 * @param line from a csv
	 * @return a list of tokens from the csv record
	 */
	private List<String> getRecordFromLine(String line) {
	  List<String> values = new ArrayList<>();
	  Scanner rowScanner = new Scanner(line);
	  rowScanner.useDelimiter(",");
	  while (rowScanner.hasNext()) {
	    values.add(rowScanner.next());
	  }
	  rowScanner.close();
	  return values;
	}
	
	/**
	 * Read the instructors.csv file, checking for duplicates and populating the List.
	 * Duplicate instructor names are not allowed.
	 * One instructor per line, first element is the name.
	 */
	private void readInstructors() {
	  Set<String> instructorNames = new HashSet<String>();
	  
		for (List<String> record : readCSV(dataDir + "/instructors.csv")) {
      // instructors.csv should have one instructor per line: "name"
		  if (record.size() < 1) continue;
		  String name = record.get(0);
		  if (instructorNames.contains(name)) {
		    System.out.println("Warning: duplicated instructor name in instructors.csv " + name);
		    continue; // disallow duplicate names for instructors
		  }
		  
		  instructors.add(new Instructor(name));
		}
	}
	
	/**
	 * Read the sections.csv file, checking for unknown instructors and populating the List.
	 * Duplicate section names are allowed.
	 * One section per line, first element is the instructor name, second is the section name.
	 */
	private void readSections() {
	  Map<String, Instructor> instructorNames = new HashMap<String, Instructor>();
	  for (Instructor i : instructors) {
	    instructorNames.put(i.getName(), i);
	  }
	  
		for (List<String> record : readCSV(dataDir + "/sections.csv")) {
      // sections.csv should have one section per line: "instructor,name"
		  if (record.size() < 2) continue;
		  String instructorName = record.get(0);
		  String name = record.get(1);
		  
		  if (!instructorNames.containsKey(instructorName)) {
		    System.out.println("Warning: unknown instructor name in sections.csv " + instructorName);
		    continue;
		  }
		  
		  Instructor inst = instructorNames.get(instructorName);
		  Section s = new Section(inst, name);
		  inst.getSections().add(s);
		  sections.add(s);
		}
	}
	
	/**
	 * Read the rooms.csv file, checking for duplicates and populating the List.
	 * Duplicate room names are not allowed.
	 * One room per line, first element is the room name.
	 */
	private void readRooms() {
		Set<String> roomNames = new HashSet<String>();
		
		for (List<String> record : readCSV(dataDir + "/rooms.csv")) {
		  // rooms.csv should have one room per line: "name"
		  if (record.size() < 1) continue;
		  String name = record.get(0);
		  if (roomNames.contains(name)) {
		    System.out.println("Warning: duplicated room name in rooms.csv " + name);
		    continue; // disallow duplicate names for rooms
		  }
		  
		  rooms.add(new Room(name));
		}
	}

	/**
	 * whether to allow negative weights.
	 * The optimization algorithm assumes weights are non-negative, so this should probably be set to false.
	 * If set to false, constraints with negative weights are ignored by readConstraints.
	 */
	private static boolean ALLOW_NEGATIVE_WEIGHTS = false;
	
	/**
	 * Read the constraints.csv file, checking for improper inputs and populating the List.
	 * A single line may define multiple constraints, since multiple sections may have the same name.
	 * The first element is the constraint name, which must be one of the following: 
	 * MatchingPrepBlock, PrepBlockEachDay, SkipPrepBlockEachDay, SectionsNotSameBlock, 
	 * SectionsNotSameDay, SectionsSameDay, SpecificBlock, SpecificPrepBlock, SpecificRoom,
	 * or InstructorPreferredRoom.
	 * The SkipPrepBlockEachDay name prevents the Schedule from autogenerating a PrepBlockEachDay constraint
	 * for the named Instructor.
	 * The InstructorPreferredRoom name creates a SpecificRoom Constraint for each Section taught by
	 * the instructor.
	 * Otherwise, each of these constructs an appropriate Constraint and registers it.
	 * The second element is the weight, and the following depend on the type of Constraint.
	 */
	private void readConstraints() {
	  skipPrepBlockEachDayNames = new HashSet<String>();
	  
	  Map<String, Instructor> instructorNames = new HashMap<String, Instructor>();
	  for (Instructor i : instructors) {
	    instructorNames.put(i.getName(), i);
	  }
	  
	  // there can be multiple sections with the same name
	  // we need to generate a constraint for each one of those duplicate sections
	  Map<String, ArrayList<Section>> sectionNames = new HashMap<String, ArrayList<Section>>();
	  for (Section s : sections) {
	    String name = s.getName();
	    if (!sectionNames.containsKey(name)) {
	       sectionNames.put(name, new ArrayList<Section>());
	    }
	    sectionNames.get(name).add(s);
	  }
	  
	  Map<String, Room> roomNames = new HashMap<String, Room>();
	  for (Room r : rooms) {
	    roomNames.put(r.getName(), r);
	  }

	  for (List<String> record : readCSV(dataDir + "/constraints.csv")) {
	    // constraints.csv should have one constraint per line
	    // first element is constraint name, second is the weight
	    if (record.size() < 2) continue;
	    String constraintName = record.get(0);
	    int weight = 0;
	    try {
	      weight = Integer.parseInt(record.get(1));
	    } catch (NumberFormatException e) {
	      System.out.println("Warning: unparseable weight in constraints.csv " + record.get(1));
	      continue;
	    }
	    
	    if (!ALLOW_NEGATIVE_WEIGHTS && weight < 0) {
	      System.out.println("Warning: negative weights are not allowed. This constraint will be ignored. " + weight);
	      continue;
	    }
	    
	    switch(constraintName) {
	      case "MatchingPrepBlock":
	        // every element after the weight is an instructor's name
	        if (record.size() < 3) continue;
	        ArrayList<Instructor> constraintInstructors = new ArrayList<>();
	        for (int i = 2; i < record.size(); i++) {
	          String name = record.get(i);
	          if (name.equals("")) continue;
	          if (!instructorNames.containsKey(name)) {
	            System.out.println("Warning: unknown instructor name in constraints.csv " + name);
	            continue;
	          }
	          constraintInstructors.add(instructorNames.get(name));
	        }
	        constraints.add(new MatchingPrepBlockConstraint(constraintInstructors, weight));
	        break;
	        
	      case "PrepBlockEachDay":
	        // third element is the instructor's name
	        if (record.size() < 3) continue;
	        String name = record.get(2);
	        if (!instructorNames.containsKey(name)) {
	          System.out.println("Warning: unkown instructor name in constraints.csv " + name);
	          continue;
	        }
	        constraints.add(new PrepBlockEachDayConstraint(instructorNames.get(name), weight));
	        break;
	        
	      case "SkipPrepBlockEachDay":
          // third element is the instructor's name
          if (record.size() < 3) continue;
          name = record.get(2);
          if (!instructorNames.containsKey(name)) {
            System.out.println("Warning: unkown instructor name in constraints.csv " + name);
            continue;
          }
          skipPrepBlockEachDayNames.add(name);
          break;
          
	      case "SectionsNotSameBlock":
	        // third and fourth elements are sections
	        if (record.size() < 4) continue;
	        String section1Name = record.get(2);
	        String section2Name = record.get(3);
          if (!sectionNames.containsKey(section1Name)) {
            System.out.println("Warning: unknown section name in constraints.csv " + section1Name);
            continue;
          }         
          if (!sectionNames.containsKey(section2Name)) {
            System.out.println("Warning: unknown section name in constraints.csv " + section2Name);
            continue;
          }
          // create a single constraint for each pair (s1, s2)
          for (Section s1 : sectionNames.get(section1Name)) {
            for (Section s2 : sectionNames.get(section2Name)) {
              constraints.add(new SectionsNotSameBlockConstraint(s1, s2, weight));
            }
          }
          break;
          
	      case "SectionsNotSameDay":
	        // third and fourth elements are sections
	        if (record.size() < 4) continue;
          section1Name = record.get(2);
          section2Name = record.get(3);
          if (!sectionNames.containsKey(section1Name)) {
            System.out.println("Warning: unknown section name in constraints.csv " + section1Name);
            continue;
          }         
          if (!sectionNames.containsKey(section2Name)) {
            System.out.println("Warning: unknown section name in constraints.csv " + section2Name);
            continue;
          }
          // create a single constraint for each pair (s1, s2)
          for (Section s1 : sectionNames.get(section1Name)) {
            for (Section s2 : sectionNames.get(section2Name)) {
              constraints.add(new SectionsNotSameDayConstraint(s1, s2, weight));
            }
          }
          break;
          
	      case "SectionsSameDay":
          // every element after the weight is a section name
          if (record.size() < 3) continue;
          ArrayList<Section> constraintSections = new ArrayList<>();
          for (int i = 2; i < record.size(); i++) {
            name = record.get(i);
            if (name.equals("")) continue;
            if (!sectionNames.containsKey(name)) {
              System.out.println("Warning: unknown section name in constraints.csv " + name);
              continue;
            }
            constraintSections.addAll(sectionNames.get(name));
          }
          constraints.add(new SectionsSameDayConstraint(constraintSections, weight));
          break;
          
	      case "SpecificBlock":
	        // third element is section name
	        // everything after the third is an allowed block
	        if (record.size() < 4) continue;
	        ArrayList<Block> allowedBlocks = new ArrayList<>();
	        name = record.get(2);
	        if (!sectionNames.containsKey(name)) {
	          System.out.println("Warning: unknown section name in constraints.csv " + name);
	          continue;
	        }
	        for (int i = 3; i < record.size(); i++) {
	          if (record.get(i).equals("")) continue;
	          allowedBlocks.add(Block.fromString(record.get(i)));
	        }
	        for (Section s : sectionNames.get(name)) {
	          constraints.add(new SpecificBlockConstraint(s, allowedBlocks, weight));
	        }
	        break;
	        
	      case "SpecificPrepBlock":
	        // third element is instructor name
	        // fourth element is block
	        if (record.size() < 4) continue;
	        name = record.get(2);
	        String blockString = record.get(3);
	        if (!instructorNames.containsKey(name)) {
	          System.out.println("Warning: unknown instructor name in constraints.csv " + name);
	          continue;
	        }
	        Block b = Block.fromString(blockString);
	        constraints.add(new SpecificPrepBlockConstraint(instructorNames.get(name), b, weight));
	        break;
	        
	      case "SpecificRoom":
	        // third element is section name
	        // everything after the third is an allowed room
	        if (record.size() < 4) continue;
	        name = record.get(2);
	        if (!sectionNames.containsKey(name)) {
	          System.out.println("Warning: unknown section name in constraints.csv " + name);
	          continue;
	        }
	        ArrayList<Room> allowedRooms = new ArrayList<>();
	        for (int i = 3; i < record.size(); i++) {
	          if (record.get(i).equals("")) continue;
	          String roomName = record.get(i);
	          if (!roomNames.containsKey(roomName)) {
	            System.out.println("Warning: unknown room name in constraints.csv " + roomName);
	            continue;
	          }
	          allowedRooms.add(roomNames.get(roomName));
	        }
	        for (Section s : sectionNames.get(name)) {
	          constraints.add(new SpecificRoomConstraint(s, allowedRooms, weight));
	        }
	        break;
	        
	      case "InstructorPreferredRoom":
	        // add a SpecificRoomConstraint for each Section taught by the specified Instructor
	        // third element is the instructor name
	        // everything after the third is an allowed room
	        if (record.size() < 4) continue;
	        String instructorName = record.get(2);
          if (!instructorNames.containsKey(instructorName)) {
            System.out.println("Warning: unknown instructor name in constraints.csv " + instructorName);
            continue;
          }
          allowedRooms = new ArrayList<>();
          for (int i = 3; i < record.size(); i++) {
            if (record.get(i).equals("")) continue;
            String roomName = record.get(i);
            if (!roomNames.containsKey(roomName)) {
              System.out.println("Warning: unknown room name in constraints.csv " + roomName);
              continue;
            }
            allowedRooms.add(roomNames.get(roomName));
          }
	        Instructor inst = instructorNames.get(instructorName);
	        for (Section s : inst.getSections()) {
	          constraints.add(new SpecificRoomConstraint(s, allowedRooms, weight));
	        }
	        break;
	        
	      default:
	        System.out.println("Warning: unknown constraint name in constraints.csv " + constraintName);
	    }
	  }
	}
	
	/**
	 * whether to generate a PrepBlockEachDay constraint for each Instructor.
	 */
	private static boolean AUTOGENERATE_PREPBLOCKEACHDAY = true;
	/**
	 * the weight to associate with autogenerated PrepBlockEachDay constraints.
	 */
	private static int AUTOGENERATE_PREPBLOCKEACHDAY_WEIGHT = 1000;
	/**
	 * whether to generate a SectionsNotSameBlock constraint for each pair of Sections taught by each Instructor.
	 * The Room class prevents multiple Sections from being scheduled at the same time in the same Room,
	 * but it is possible for an Instructor to be scheduled to teach multiple Sections simultaneously.
	 * Setting this to true will prevent that from happening.
	 */
	private static boolean AUTOGENERATE_SECTIONSNOTSAMEBLOCK = true;
	/**
	 * the weight to associate with autogenerated SectionsNotSameBlock constraints
	 *  -- should be large relative to other constraints.
	 */
	private static int AUTOGENERATE_SECTIONSNOTSAMEBLOCK_WEIGHT = 10000;
	
	/**
	 * automatically generate constraints for each instructor.
	 * @see AUTOGENERATE_PREPBLOCKEACHDAY
	 * @see AUTOGENERATE_SECTIONSNOTSAMEBLOCK
	 */
	private void autogenerateConstraints() {
	  if (AUTOGENERATE_PREPBLOCKEACHDAY) {
	    for (Instructor i : instructors) {
	      if (skipPrepBlockEachDayNames.contains(i.getName())) continue;
	      constraints.add(new PrepBlockEachDayConstraint(i, AUTOGENERATE_PREPBLOCKEACHDAY_WEIGHT));
	    }
	  }
	  
	  if (AUTOGENERATE_SECTIONSNOTSAMEBLOCK) {
	    for (Instructor i : instructors) {
	      ArrayList<Section> instSections = i.getSections();
	      for (int j = 0; j < instSections.size() - 1; j++) {
	        for (int k = j + 1; k < instSections.size(); k++) {
	          constraints.add(new SectionsNotSameBlockConstraint(instSections.get(j), instSections.get(k), AUTOGENERATE_SECTIONSNOTSAMEBLOCK_WEIGHT));
	        }
	      }
	    }
	  }
	}
	
	/**
	 * @return a CSV string representing the schedule, organized by Instructor
	 */
	public String scheduleByInstructorCSV() {
	  String retval = "instructor,";

	  for (int i = 0; i < Block.NBLOCKS; i++) {
	    retval += Block.valueOf(i).toString() + ",";
	  }
	  // remove trailing comma
	  retval = retval.substring(0, retval.length() - 1);
	  retval += "\n";
	  
	  for (Instructor inst : instructors) {
	    retval += inst.getName() + ",";
	    for (int j = 0; j < Block.NBLOCKS; j++) {
	      Section s = inst.getSection(Block.valueOf(j));
	      if (s == null) {
	        retval += ",";
	      } else {
	        retval += s.getName() + " (" + s.getRoom().getName() + "),";
	      }
	    }
	    // remove trailing comma
	    retval = retval.substring(0, retval.length() - 1);
	    retval += "\n";
	  }
	  
	  return retval;
	}
	
	/**
	 * @return a CSV string representing the schedule, organized by Room
	 */
	public String scheduleByRoomCSV() {
	  String retval = "room,";
	  
	  for (int i = 0; i < Block.NBLOCKS; i++) {
	    retval += Block.valueOf(i).toString() + ",";
	  }
	  // remove trailing comma
    retval = retval.substring(0, retval.length() - 1);
	  retval += "\n";
	  
	  for (Room r : rooms) {
	    retval += r.getName() + ",";
	    for (int j = 0; j < Block.NBLOCKS; j++) {
	      Section s = r.getSection(Block.valueOf(j));
	      if (s == null) {
	        retval += ",";
	      } else {
	        retval += s.getName() + " (" + s.getInstructor().getName() + "),";
	      }
	    }
	    // remove trailing comma
	    retval = retval.substring(0, retval.length() - 1);
	    retval += "\n";
	  }
	  
	  return retval;
	}
	
	/**
	 * @return a CSV string summarizing the Sections, sorted by decreasing cost
	 */
	public String sectionSummaryCSV() {
	  Collections.sort(sections, new SortByCost(SortByCost.SortDirection.DESCENDING));
	  
	  String retval = "section name,instructor,room,block,totalCost\n";

	  for (Section s : sections) {
	    retval += s.getName() + "," + s.getInstructor().getName() + ",";
	    if (s.getRoom() == null) {
	      retval += ",";
	    } else {
	      retval += s.getRoom().getName() + ",";
	    }
	    retval += s.getBlock() + "," + s.totalCost() + "\n";
	  }
	  
	  return retval;
	}
	
	/**
	 * @return a CSV string summarizing the Instructors, sorted by decreasing cost
	 */
	public String instructorSummaryCSV() {
	  Collections.sort(instructors, new SortByCost(SortByCost.SortDirection.DESCENDING));
	  
	  String retval = "instructor name,totalCost\n";
	  
	  for (Instructor i : instructors) {
	    retval += i.getName() + "," + i.totalCost() + "\n";
	  }
	  
	  return retval;
	}
	
	/**
	 * @return a CSV string summarizing the Constraints, sorted by decreasing weight
	 */
	public String constraintSummaryCSV() {
	  Collections.sort(constraints, new SortByWeight(SortByWeight.SortDirection.DESCENDING));
	  
	  String retval = "description,is violated,weight\n";
	  
	  for (Constraint c : constraints) {
	    retval += "\"" + c.description() + "\",";
	    if (c.isViolated()) {
	      retval += "violated,";
	    } else {
	      retval += "met,";
	    }
	    retval += c.weight() + "\n";
	  }
	  
	  return retval;
	}
}
