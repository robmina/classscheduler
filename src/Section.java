import java.util.ArrayList;
import java.util.Objects;

/**
 * a section of a class, taught by an Instructor.
 * The Schedule consists of Sections assigned to Rooms and Blocks.
 */
public class Section implements Costs {
	/**
	 * a control designed to simulate the C++ friend keyword.
	 * Taken from https://stackoverflow.com/a/18634125
	 */
	public static final class SectionAssignControl { private SectionAssignControl() {} }
	private static final SectionAssignControl control = new SectionAssignControl();
	
	/**
	 * a redundant reference to the Instructor who teaches this Section.
	 * It is redundant with the reference stored in the Instructor object.
	 */
	private Instructor instructor;
	private Block block;
	/**
	 * a redundant reference to the Room to which this Section is assigned.
	 * It is redundant with the reference stored in the Room object.
	 */
	private Room room;
	/**
	 * all Constraints on the Schedule that arise directly from the assignment of this Section.
	 * This is used for convenience to help in estimating the most difficult Sections.
	 */
	private ArrayList<Constraint> constraints;
	/**
	 * Section names are not unique.
	 */
	private String name;
	
	/**
	 * @param instructor the instructor
	 * @param name the non-unique name
	 */
	public Section(Instructor instructor, String name) {
		this.instructor = instructor;
		this.block = Block.UNASSIGNED;
		this.room = null;
		this.constraints = new ArrayList<>();
		this.name = name;
	}
	
	public String getName() { return name; }
	public Instructor getInstructor() { return instructor; }
	public Block getBlock() { return block; }
	public Room getRoom() { return room; }
	public ArrayList<Constraint> getConstraints() { return constraints; }
	
	/**
	 * the total cost associated with the Constraints directly related to this Section.
	 * This is not used directly in the optimization, but as a convenience for
	 * ranking Sections by difficulty.
	 */
	public int totalCost() { 
		int cost = 0;
		for ( Constraint c : constraints ) {
			if (c.isViolated()) cost += c.weight();
		}
		return cost;
	}

	@Override
	public String toString() {
		return "Section of " + name + " (" + instructor + ")";
	}
	
	/**
	 * @param c a control that prevents this method from being called except by the Room class
	 * @param r the Room to which this Section should be assigned
	 * @param b the Block to which this Section should be assigned
	 */
	public void assignRoomAndBlock(Room.RoomAssignControl c, Room r, Block b) {
		Objects.requireNonNull(c);
		
		this.room = r;
		this.block = b;
		
		instructor.assignBlock(control, this, b);
	}
	
	/**
	 * @param c a control that prevents this method from being called except by the Room class
	 */
	public void unassignRoomAndBlock(Room.RoomAssignControl c) {
		Objects.requireNonNull(c);
		
		this.room = null;
		this.block = Block.UNASSIGNED;
		
		instructor.unassignBlock(control, this);
	}
	
	/**
	 * called from the constructor of the Constraint class, not from Schedule
	 * @param c the Constraint to be added to the list.
	 */
	public void addConstraint(Constraint c) {
	  constraints.add(c);
	}
}
