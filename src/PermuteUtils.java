import java.util.ArrayList;
import java.util.stream.LongStream;

/**
 * utility class to compute combinations and permutations for the optimization algorithm
 */
public class PermuteUtils {
	/**
	 * a tabulated implementation of factorial -- results are hard-coded
	 * @param n the input to the factorial which must be in [0, 20] (inclusive)
	 * @return n * (n-1) * (n-2) * ... * 2 * 1
	 */
	public static long factorial(int n) {
		if (n < 0 || n > 20) {
			return -1;
		}
		// https://www.baeldung.com/java-calculate-factorial
		//return LongStream.rangeClosed(1, n)
		//		.reduce(1, (long x, long y) -> x * y);
		
		switch(n) {
			case 0:
				return 1L;
			case 1:
				return 1L;
			case 2:
				return 2L;
			case 3:
				return 6L;
			case 4:
				return 24L;
			case 5:
				return 120L;
			case 6:
				return 720L;
			case 7:
				return 5040L;
			case 8:
				return 40320L;
			case 9:
				return 362880L;
			case 10:
				return 3628800L;
			case 11:
				return 39916800L;
			case 12:
				return 479001600L;
			case 13:
				return 6227020800L;
			case 14:
				return 87178291200L;
			case 15:
				return 1307674368000L;
			case 16:
				return 20922789888000L;
			case 17:
				return 355687428096000L;
			case 18:
				return 6402373705728000L;
			case 19:
				return 121645100408832000L;
			case 20:
				return 2432902008176640000L;
			default:
				return -1;
		}
	}

	/**
	 * implementation adapted from https://www.keithschwarz.com/interesting/code/?dir=factoradic-permutation
	 * @param <T> the Type of the List elements
	 * @param original the original order of the List (0th permutation) of length n
	 * @param permutationNumber a number in [0, n!) that uniquely identifies a permutation of the input List
	 * @return the identified permutation
	 */
	public static <T> ArrayList<T> getPermutation(ArrayList<T> original, long permutationNumber) {
		if (original.size() > 20) {
			return null;
		}
		if (permutationNumber >= factorial(original.size())) {
			return null;
		}
		
		if (original.size() <= 1) return (ArrayList<T>) original.clone();
		
		original = (ArrayList<T>) original.clone();

		// see the implementation of NthPermutation here: https://www.keithschwarz.com/interesting/code/?dir=factoradic-permutation
		int k = original.size();
		ArrayList<T> retval = new ArrayList<>(k);
		long radix = factorial(k - 1);
		for (int i = 0; i < k; i++) {
			long digit = permutationNumber / radix;
			
			retval.add(original.get((int) digit));
			
			original.remove((int) digit);
			
			permutationNumber %= radix;
			
			if (i + 1 != k) {
				radix /= (k - i - 1);
			}
		}
		
		return retval;
	}
	
	/**
	 * @param n the total number of items to choose from
	 * @param k the number of items to choose
	 * @return n Choose k -- the number of combinations of k items from a total of n
	 */
	public static long binomial(int n, int k) {
		return factorial(n) / (factorial(k) * factorial(n - k));
	}
	
	/**
	 * implementation adapted from https://www.reddit.com/r/algorithms/comments/4o5a9x/unique_mapping_from_integer_to_nk_combination/
	 * @param n the total number of items to choose from
	 * @param k the number of items to choose
	 * @param m uniquely identifies one of the n Choose k possible combinations
	 * @return a List of k indices in [0, n) representing the mth possible combination
	 */
	public static ArrayList<Integer> getMthCombinationIndices(int n, int k, long m) {
		ArrayList<Integer> retval = new ArrayList<Integer>(k);
		
		if (n < 1 || k < 1) return retval;
		
		long n_choose_k = binomial(n, k);
		
		if (m >= n_choose_k) return retval;
		
		m = n_choose_k - 1 - m;
		
		int guess = n - 1;
		
		for (int i = k; i >= 1; i--) {
			long take = binomial(guess, i);
			while (take > m) {
				guess = guess - 1;
				take = binomial(guess, i);
			}
			
			m = m - take;
			
			retval.add(n - 1 - guess);
		}
		
		return retval;
	}
	
	/**
	 * @param <T> the Type of the List elements
	 * @param original the elements to choose from of size n
	 * @param comboSize the number of elements to choose
	 * @param combinationNumber uniquely identifies one of the n Choose comboSize possible combinations 
	 * @return a List of size comboSize representing the combinationNumberth possible combination
	 */
	public static <T> ArrayList<T> getCombination(ArrayList<T> original, int comboSize, long combinationNumber) {
		ArrayList<Integer> indices = getMthCombinationIndices(original.size(), comboSize, combinationNumber);
		
		ArrayList<T> retval = new ArrayList<T>(comboSize);
		
		for (Integer i : indices) {
			retval.add(original.get(i));
		}
		
		return retval;
	}
	
}
