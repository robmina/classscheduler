import java.util.ArrayList;

/**
 * A Constraint on the Schedule may be violated, costs a certain weight if it is violated,
 * is relevant for certain Instructors and/or Sections, and has a description.
 */
public interface Constraint {

	public boolean isViolated();
	
	public int weight();
	
	public ArrayList<Instructor> instructors();
	
	public ArrayList<Section> sections();
	
	public String description();
	
}
