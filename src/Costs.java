
/**
 * Something that has a total cost.
 */
public interface Costs {

	public int totalCost();
	
}

