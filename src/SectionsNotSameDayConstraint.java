import java.util.ArrayList;

/**
 * certain Sections should not be scheduled on the same day.
 * For example, the two Sections of BC Calculus (double-blocked) should occur on different days.
 * Note: a Constraint will be created for each instance of Section with the same name as specified in the constraints.csv file.
 */
public class SectionsNotSameDayConstraint implements Constraint {

	private Section section1;
	private Section section2;
	private int weight;
	
	public SectionsNotSameDayConstraint(Section section1, Section section2, int weight) {
		this.section1 = section1;
		this.section2 = section2;
		this.weight = weight;
		
		section1.addConstraint(this);
		section1.getInstructor().addConstraint(this);
		section2.addConstraint(this);
		section2.getInstructor().addConstraint(this);
	}
	
	/**
	 * @return true if both sections are scheduled on A days or both on B days
	 */
	public boolean isViolated() {
		if (section1 == null || section2 == null) return false;
		
		return (section1.getBlock().isADay() && section2.getBlock().isADay()) ||
			   (section1.getBlock().isBDay() && section2.getBlock().isBDay());
	}

	public int weight() {
		return weight;
	}

	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<>();
		
		if (section1 != null && section1.getInstructor() != null)
			retval.add(section1.getInstructor());
		
		if (section2 != null && section2.getInstructor() != null)
			retval.add(section2.getInstructor());
		
		return retval;
	}

	public ArrayList<Section> sections() {
		ArrayList<Section> retval = new ArrayList<>();
		
		if (section1 != null)
			retval.add(section1);
		
		if (section2 != null)
			retval.add(section2);
		
		return retval;
	}

	public String description() {		
		return "The following sections should not be on the same day: " + section1 + " and " + section2;
	}

}
