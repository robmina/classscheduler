import java.util.ArrayList;

/**
 * most Instructors should have at least one planning period (prep) on A days and on B days.
 */
public class PrepBlockEachDayConstraint implements Constraint {
	private Instructor instructor;
	private int weight;
	
	public PrepBlockEachDayConstraint(Instructor instructor, int weight) {
		this.instructor = instructor;
		this.weight = weight;
		
		instructor.addConstraint(this);
	}
	
	/**
	 * @return true if the Instructor has no planning period on A days and/or no planning period on B days
	 */
	public boolean isViolated() {
		if (instructor == null) return false;
		
		ArrayList<Block> prepBlocks = instructor.freeBlocks();
		return (!(prepBlocks.contains(Block.A1) || prepBlocks.contains(Block.A2) || prepBlocks.contains(Block.A3) || prepBlocks.contains(Block.A4)) ||
				!(prepBlocks.contains(Block.B1) || prepBlocks.contains(Block.B2) || prepBlocks.contains(Block.B3) || prepBlocks.contains(Block.B4)));
	}
	
	public int weight() { return weight; }
	
	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<Instructor>();
		if (instructor != null)
			retval.add(instructor);
		return retval;
	}
	
	public ArrayList<Section> sections() {
		return new ArrayList<Section>();
	}
	
	public String description() {
		if (instructor == null) return "";
		
		return "" + instructor + " should have at least one prep block on A days and B days.";
	}
	
}
