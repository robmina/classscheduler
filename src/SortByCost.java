import java.util.Comparator;

/**
 * Helper class to sort Costs objects in either ascending or descending order of totalCost
 */
public class SortByCost implements Comparator<Costs> {
	
	public enum SortDirection {
		ASCENDING,
		DESCENDING
	}
	
	private SortDirection dir;
	
	public SortByCost(SortDirection dir) {
		this.dir = dir;
	}
	
	public int compare(Costs a, Costs b) {
		if (dir == SortDirection.ASCENDING)
			return a.totalCost() - b.totalCost();
		return b.totalCost() - a.totalCost();
	}
}