import java.util.ArrayList;

/**
 * certain Sections should be scheduled during a particular Block or set of Blocks.
 * Note: a Constraint will be created for each instance of Section with the same name as specified in the constraints.csv file.
 * If there are more Sections than there are allowed Blocks, then this Constraint must necessarily be violated.
 */
public class SpecificBlockConstraint implements Constraint {

	private Section section;
	private ArrayList<Block> allowedBlocks;
	private int weight;

	public SpecificBlockConstraint(Section section, ArrayList<Block> allowedBlocks, int weight) {
		this.section = section;
		this.allowedBlocks = allowedBlocks;
		this.weight = weight;
		
		this.section.getInstructor().addConstraint(this);
		this.section.addConstraint(this);
	}

	/**
	 * @return true if the Section is not assigned to one of the allowed Blocks
	 */
	public boolean isViolated() {
		if (section == null || allowedBlocks == null) return false;
		return !allowedBlocks.contains(section.getBlock());
	}

	public int weight() {
		return weight;
	}

	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<Instructor>();
		if (section != null && section.getInstructor() != null)
			retval.add(section.getInstructor());
		return retval;
	}

	public ArrayList<Section> sections() {
		ArrayList<Section> retval = new ArrayList<Section>();
		if (section != null)
			retval.add(section);
		return retval;
	}

	public String description() {
		if (section == null || allowedBlocks == null) return "";
		
		String retval = section + " should be scheduled during one of the following blocks: ";
		for (int i = 0; i < allowedBlocks.size() - 1; i++) {
			retval += allowedBlocks.get(i).toString() + ", ";
		}
		if (allowedBlocks.size() > 0) {
			if (allowedBlocks.size() > 1)
				retval += "and ";
			retval += allowedBlocks.get(allowedBlocks.size() - 1).toString();
		}
		
		return retval;
	}

}
