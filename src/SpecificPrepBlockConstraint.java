import java.util.ArrayList;

/**
 * certain Instructors should have a planning period (prep) during certain Blocks.
 */
public class SpecificPrepBlockConstraint implements Constraint {

	private Instructor instructor;
	private Block block;
	private int weight;
	
	public SpecificPrepBlockConstraint(Instructor instructor, Block block, int weight) {
	  this.instructor = instructor;
	  this.block = block;
	  this.weight = weight;
	  
	  this.instructor.addConstraint(this);
	}
	
	/**
	 * @return true if the Instructor does not have a planning period during the specified Block
	 */
	public boolean isViolated() {
		if (instructor == null || block == Block.UNASSIGNED)
			return false;
		
		return !instructor.freeBlocks().contains(block);
	}

	public int weight() {
		return weight;
	}

	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<>();
		
		if (instructor != null)
			retval.add(instructor);
		
		return retval;
	}

	public ArrayList<Section> sections() {
		return new ArrayList<Section>();
	}

	public String description() {
		return instructor + " should have a prep period during " + block;
	}

}
