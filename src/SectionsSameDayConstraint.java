import java.util.ArrayList;

/**
 * certain Sections should be scheduled on the same day.
 * Note: a Constraint will be created for each instance of Section with the same name as specified in the constraints.csv file.
 * If there are more Sections with the same name than there are Blocks during a particular day, this constraint must necessarily be violated.
 */
public class SectionsSameDayConstraint implements Constraint {

	private ArrayList<Section> sections;
	private int weight;
	
	public SectionsSameDayConstraint(ArrayList<Section> sections, int weight) {
		this.sections = sections;
		this.weight = weight;
		
		for (Section s : sections) {
		  s.addConstraint(this);
		  s.getInstructor().addConstraint(this);
		}
	}
	
	/**
	 * @return true if the all of the Sections are not scheduled on the same day
	 */
	public boolean isViolated() {
		if (sections == null || sections.size() <= 1) return false;
		if (sections.size() > 4) return true;
		
		boolean allADay = true;
		boolean allBDay = true;
		for (Section s : sections) {
			if (s == null) continue;
			
			if (s.getBlock().isADay()) {
				allBDay = false;
			} else if (s.getBlock().isBDay()) {
				allADay = false;
			} else { // unassigned
				allADay = false;
				allBDay = false;
			}
		}
		return !(allADay || allBDay);
	}

	public int weight() {
		return weight;
	}

	public ArrayList<Instructor> instructors() {
		ArrayList<Instructor> retval = new ArrayList<Instructor>();
		
		if (sections == null) return retval;
		
		for (Section s : sections) {
			if (s == null || s.getInstructor() == null) continue;
			if (!retval.contains(s.getInstructor()))
				retval.add(s.getInstructor());
		}
		
		return retval;
	}

	public ArrayList<Section> sections() {
		return sections;
	}

	public String description() {
		if (sections == null) return "";
		
		String retval = "The following sections should be on the same day: ";
		
		for (int i = 0; i < sections.size() - 1; i++) {
			retval += sections.get(i) + ", ";
		}
		if (sections.size() > 0) {
			if (sections.size() > 1)
				retval += "and ";
			retval += sections.get(sections.size() - 1);
		}
		
		return retval;
	}

}
